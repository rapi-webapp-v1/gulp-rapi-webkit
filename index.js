'use strict';

function make(c)
{
  global.rapiConfig = c;
  require('./tasks/rapi');
  require('./tasks/conf');
  require('./tasks/build-dev');
  require('./tasks/build');
  require('./tasks/inject');
  require('./tasks/server');
  require('./tasks/scripts');
  require('./tasks/styles');
  require('./tasks/watch');
}

exports.make = make;
