/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

var gutil = require('gulp-util');

var config = global.rapiConfig;

/**
 *  The main paths of your project handle these with care
 */
exports.paths = config.paths;

// exports.paths = {
//   src: 'src',
//   externalSrc: 'src/app/external',
//   dist: '../public/admin',
//   commons: '../commons',
//   plugins: '../plugins',
//   tmp: '.tmp',
//   e2e: 'e2e',
//   langs: 'src/assets/langs'
// };

/**
 * CONFIG RAPI
 */
exports.rapi = {
  commons: config.commons || [],
  plugins: config.plugins || [],
  langs: config.langs || []
};

/**
 *  Wiredep is the lib which inject bower dependencies in your project
 *  Mainly used to inject script tags in the index.html but also used
 *  to inject css preprocessor deps and js files in karma
 */
exports.wiredep = {
  directory: 'bower_components'
};

/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.errorHandler = function(title) {
  'use strict';

  return function(err) {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emit('end');
  };
};
