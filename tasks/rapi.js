'use strict';

var path        = require('path');
var gulp        = require('gulp-help')(require('gulp'));
var conf        = require('./conf');
var del         = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var mergeJson   = require('gulp-merge-json');
var debug       = require('gulp-debug');

function isOnlyChange(event){
    return event.type === 'changed';
}

/////////////////////////////////////////////////////////
/////////////////// rapi:serve //////////////////////////
/////////////////////////////////////////////////////////

gulp.task(
      'rapi:serve',
      'Watch rapi and language files and serve the application. Run rapi:pre:build first.',
      function (callback) {
    return runSequence(
        'rapi:watch:plugins',
        'rapi:watch:commons',
        'rapi:watch:lang',
        'serve',
        callback);
});

/////////////////////////////////////////////////////////
/////////////////// rapi:clean //////////////////////////
/////////////////////////////////////////////////////////

gulp.task(
      'rapi:clean',
      // 'Delete directories of shadow copy from commoms, plugins in external and language in assets.',
      'Delete files imported from plugins, commons modules and merged language files.',
      function (callback) {
    return runSequence(
        'rapi:clean:external',
        'rapi:clean:lang',
        callback);
})

gulp.task(
      'rapi:clean:external',
      // 'Delete directory of shadow copy from commoms and plugins ine external.',
      'Delete files imported from plugins and commons modules.',
      function () {
    return del([
        path.join(conf.paths.externalSrc, '/**/*'),
        path.join('!' + conf.paths.externalSrc, '.gitignore')
    ]);
});

gulp.task(
      'rapi:clean:lang',
      // 'Delete directory of shadow copy from language in assets.',
      'Delete merged language files.',
      function () {
    return del([
        path.join(conf.paths.langs, '/**/*'),
        path.join('!' + conf.paths.langs, '.gitignore')
    ]);
});

/////////////////////////////////////////////////////////
////////////////////// rapi:pre /////////////////////////
/////////////////////////////////////////////////////////

gulp.task(
      'rapi:pre:build',
      'Refresh files from plugins, commons module and merged language files.',
      function (callback) {
    return runSequence(
        'rapi:clean',
        'rapi:import',
        callback)
});

/////////////////////////////////////////////////////////
/////////////////// rapi:import /////////////////////////
/////////////////////////////////////////////////////////

gulp.task(
      'rapi:import',
      'Import all files into the project',
      function (callback) {
    gulp.start('rapi:import:plugins');
    gulp.start('rapi:import:commons')
    return gulp.start('rapi:import:lang');
});

gulp.task(
      'rapi:import:lang',
      'Merge all languages JSON files from application to a single one per language in assets.',
      function () {
  var langs = conf.rapi.langs;
  var paths = [];
  var stream;

  console.log('Total languages to import: ' + conf.rapi.langs.length);

  for (var i = 0; i < conf.rapi.langs.length; i++) {
      var language = conf.rapi.langs[i];

      // All langs files are inside of a 'il8n' directory
      var langFile = '/**/**/i18n/' + language + '.json';

      // make sure of empty paths in every iteration
      paths = [];

      // add langs paths from Admin
      paths.push(path.join(conf.paths.src, '/app/main' + langFile));

      // add langs paths from plugins
      for (var j = 0; j < conf.rapi.plugins.length; j++) {
          paths.push(path.join(conf.paths.plugins, conf.rapi.plugins[j], langFile));
      }

      // add langs paths from commons
      for (var j = 0; j < conf.rapi.commons.length; j++) {
          paths.push(path.join(conf.paths.commons, conf.rapi.commons[j], langFile));
      }

      // Loggins language and paths
      console.log('\n');
      console.log('Importing language [' + language + ']. Present at:');
      for (var j = 0; j < paths.length; j++) {
          console.log('\tLanguage src path: ' + paths[j]);
      }

      stream = gulp
          .src(paths)
          .pipe(mergeJson({
              fileName: language + '.json',
          }))
          .pipe(gulp.dest(path.join(conf.paths.langs,'/i18n')));
    }
    return stream;
});

gulp.task(
      'rapi:import:commons',
      'Import all modules defined in configuration file in src/app/external directory.',
      function () {
    var stream;
    var src, dest, common;

    console.log('Total modules to import: ' + conf.rapi.commons.length);

    for (var i = 0; i < conf.rapi.commons.length; i++) {
        common = conf.rapi.commons[i];
        src   = path.join(conf.paths.commons, common, '/**/*');
        dest  = path.join(conf.paths.externalSrc, common);

        console.log('\n');
        console.log('Importing commons module [' + common + '].');
        console.log('\tCommons src path:  ' + src);
        console.log('\tCommons dest path: ' + dest);

        stream = gulp
            .src(src)
            .pipe(gulp.dest(dest));
    }
    return stream;
});

gulp.task(
      'rapi:import:plugins',
      'Import all plugins defined in configuration file in src/app/external directory.',
      function () {
    var stream;
    var src, dest, plugin;

    console.log('Plugins to import: ' + conf.rapi.plugins.length);

    for (var i = 0; i < conf.rapi.plugins.length; i++) {
        plugin = conf.rapi.plugins[i];
        src   = path.join(conf.paths.plugins, plugin, '/src/**/*');
        dest  = path.join(conf.paths.externalSrc, plugin);

        console.log('\n');
        console.log('Importing plugin [' + plugin + '].');
        console.log('\tPlugin src path:  ' + src);
        console.log('\tplugin dest path: ' + dest);

        stream = gulp
            .src(src)
            .pipe(gulp.dest(dest));
    }
    return stream;
});

/////////////////////////////////////////////////////////
/////////////////// rapi:watch //////////////////////////
/////////////////////////////////////////////////////////


gulp.task(
      'rapi:watch:lang',
      'Watch files in app/src directory for changes.',
      function () {
    var language = '', paths = [];
    console.log('Languages to watch: ' + conf.rapi.langs.length);

    for (var i = 0; i < conf.rapi.langs.length; i++) {
        language = conf.rapi.langs[i];

        // All langs files are inside of a 'il8n' directory
        var langFile = '/**/**/i18n/' + language + '.json';

        // make sure of empty paths in every iteration
        paths = [];

        // add langs paths from Admin
        paths.push(path.join(conf.paths.src, '/app/main' + langFile));

        // add langs paths from plugins
        for (var j = 0; j < conf.rapi.plugins.length; j++) {
            paths.push(path.join(conf.paths.plugins, conf.rapi.plugins[j], langFile));
        }

        // add langs paths from commons
        for (var j = 0; j < conf.rapi.commons.length; j++) {
            paths.push(path.join(conf.paths.commons, conf.rapi.commons[j], langFile));
        }

        // Loggins language and paths
        console.log('\n');
        console.log('Watching language [' + language + ']. Present at:');
        for (var j = 0; j < paths.length; j++) {
            console.log('\tLanguage src path: ' + paths[j]);
        }
    }

    return gulp.watch(paths, function () {
        gulp.start('rapi:import:lang');
        console.log('Changed languages!!!');
    });
});

gulp.task(
      'rapi:watch:commons',
      'Watch original files in commons directory for changes. Only selected modules are watched.',
      function () {
    var module = '', paths = [], src = '';

    console.log('Modules to watch: ' + conf.rapi.commons.length);

    for (var i = 0; i < conf.rapi.commons.length; i++) {
        module = conf.rapi.commons[i];
        src = path.join(conf.paths.commons, module, '/**/*');
        paths.push(src);

        console.log('\n');
        console.log('Watching common [' + module + ']. Present at:');
        console.log('\tModule src path:  ' + src);
    }

    return gulp.watch(paths, function () {
        gulp.start('rapi:import:commons');
        console.log('Changed modules!!!');
    });
});

gulp.task(
      'rapi:watch:plugins',
      'Watch original files in plugins directory for changes. Only selected plugins are watched.',
      function () {
    var plugin = '', paths = [], src = '';

    console.log('Plugins to watch: ' + conf.rapi.plugins.length);

    for (var i = 0; i < conf.rapi.plugins.length; i++) {
        plugin = conf.rapi.plugins[i];
        src = path.join(conf.paths.plugins, plugin, '/src/**/*')
        paths.push(src);

        console.log('\n');
        console.log('Watching common [' + plugin + ']. Present at:');
        console.log('\tModule src path:  ' + src);
    }

    return gulp.watch(paths, function () {
        gulp.start('rapi:import:plugins');
        console.log('Changed plugins!!!');
    });
});
