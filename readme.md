GULP RAPI WEBKIT
================================


# Install in production
`npm i ssh:git@bitbucket.org:rapi3/gulp-rapi-webkit.git --save`

# Install in development environment

Na raiz do módulo, execute os comandos:

```  
 mkdir node_modules
 git clone ssh://git@bitbucket.org/rapi3/gulp-rapi-webkit.git node_modules/rapi-webkit   
 npm install   
```  
